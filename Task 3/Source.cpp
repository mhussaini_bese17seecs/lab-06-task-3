#include <iostream>
#include <cstdlib>
#include <ctime>
#include <vector>
#include <algorithm>
#include <time.h>
using namespace std;

void generateDiceSequence() {
	srand(time(0));
	cout << "Dice Sequence: " << endl;
	for (int i = 0; i < 6; i++) {
		cout << rand() % 7 << " ";
	}
	cout << endl;
}

//method to generate an vecotr of random integers with given size
vector<int> generateVector(int size) {
	vector<int> a;
	int randNum = 0;
	for (int i = 0; i < size; i++) {
		randNum = (rand() % 101) + 1; //range between 1-100
		a.push_back(randNum);
	}
	return a;
}

//method to print a vector
void printVector(vector<int> a) {
	for (int i = 0; i < a.size(); i++) {
		cout << a[i] << " ";
	}
	cout << endl;
}

//moveMin function with a nested for loop 
void moveMin(vector<int> &in, vector<int> & out) {
	out = in;
	int temp = 0;
	for (int i = 0; i < out.size() - 1; i++) {
		for (int j = 0; j < out.size() - 1; j++) {
			if (out[j] > out[j + 1]) {
				swap(out[j], out[j + 1]);
			}
		}
	}

}

void swap(vector<int>& v, int x, int y) {
	int temp = v[x];
	v[x] = v[y];
	v[y] = temp;

}

void quicksort(vector<int> &vec, int L, int R) {
	int i, j, mid, piv;
	i = L;
	j = R;
	mid = L + (R - L) / 2;
	piv = vec[mid];

	while (i<R || j>L) {
		while (vec[i] < piv)
			i++;
		while (vec[j] > piv)
			j--;

		if (i <= j) {
			swap(vec, i, j);
			i++;
			j--;
		}
		else {
			if (i < R)
				quicksort(vec, i, R);
			if (j > L)
				quicksort(vec, L, j);
			return;
		}
	}
}


// moveMin function with a newer algorithm (quikSort)
void moveMin2(vector<int> &in, vector<int> & out) {
	out = in;
	quicksort(out, 0, out.size() - 1);


}
//testCase for MoveMin
bool testMoveMin() {

	vector<int> a = generateVector(100); //generating an vector of size 100
	vector<int> b;
	moveMin(a, b);
	sort(a.begin(), a.end());
	int randNum = (rand() % 101) + 1;
	a.push_back(randNum);
	b.push_back(randNum);

	for (int i = 0; i < a.size(); i++) {
		if (a[i] != b[i])
			return false;
	}
	return true;
}

//testCase for MoveMin2
bool testMoveMin2() {

	vector<int> a = generateVector(100); //generating an vector of size 100
	vector<int> b;
	moveMin2(a, b);
	sort(a.begin(), a.end());
	int randNum = (rand() % 101) + 1;
	a.push_back(randNum);
	b.push_back(randNum);

	for (int i = 0; i < a.size(); i++) {
		if (a[i] != b[i])
			return false;
	}
	return true;

}


int main() {

	vector<int> a = generateVector(10000); //generating a vector of certain size 
	vector<int> b;
	vector<int> b1;

	cout << "Time Comparison for vector of size: " << a.size() << endl;

	clock_t start;
	start = clock();
	moveMin(a, b);
	cout << endl;
	cout << "Old Algorithm running time: ";
	cout << (((double)(clock() - start)) / CLOCKS_PER_SEC) << "sec" << endl;


	clock_t start1;
	start1 = clock();
	moveMin2(a, b1);
	cout << "Newer algorithm running time: ";
	cout << (((double)(clock() - start1)) / CLOCKS_PER_SEC) << "sec" << endl;



	cout << endl;
	system("pause");
	return 0;
}
